<?php
/**
 * Service by EPIC TEAM
 */

G::loadClass("pmFunctions");
$curl = curl_init();
header("Content-Type: application/json; charset=utf-8");
$server = "https://5lmnj6zx.api.processmaker.io";
$processId = "45d5ae54-f6d3-4c47-b1ef-ee15e8467d69";
$eventId = "33aed9c4-863e-459f-a7c5-d14c6ad64ff5";

$authorization = "authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImY0YjZhZDFlMWVhNDgwMjVjODg5ZjYwZTYxMGRlZmUyZmM0MmEyNDA5ZmVjMTVlNWI1NDI4M2M0NWExYmVmMDgzNDMzNGQxNjg0OWQ1NWMyIn0.eyJhdWQiOiIxIiwianRpIjoiZjRiNmFkMWUxZWE0ODAyNWM4ODlmNjBlNjEwZGVmZTJmYzQyYTI0MDlmZWMxNWU1YjU0MjgzYzQ1YTFiZWYwODM0MzM0ZDE2ODQ5ZDU1YzIiLCJpYXQiOjE0OTYxODIxMjcsIm5iZiI6MTQ5NjE4MjEyNywiZXhwIjoxNTI3NzE4MTI3LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.fpxNMA3aI0u5xuEC26-zy_WSHe6Ma_oq49veKzx258AahuzmkXZ0YjTAX4n30ADeZSmqgx2FOa1TimjCNN-5YwD3e4rr_zQvos8ASbzDbnWoVp7qANXLGCRkwoQ3l5KueHSLmqql1r91uMPiuprwApfxJsh3OLb8vN4aTSiDSPpFzSAItRknfabqACvkP7N7tkDsXXgwTP9M7rteFBS_GzWZ-gx-hP5fc-e9MrBwJbfMhpw2AgbfTX7I16gSoMprGfh6y0PwzMQocPsbgTKzCshVgkH3sPgYywO_UBp735xmsin8cfjkyaYT_x_8qNH8XNFbp1Wnrcyj2mID0108yVRATHmdp6X940sVVAWzGXyBxXqdwspNpxf_nZxgv50BZ-8t1inxxRSOTQdKZx-2aqsuLSQ06AW4_7MS3Y8SXGHr_aceRTydIiTHX69jzZ64RIdcDWpN0iF8Aqcr3HZwm2hemZLxBGYylUf9Pcs1ZXi-d3K8rf41hcvQG-jAP8XJtdxwrsirW-sI062aTxqtGuPAz9q22NRhBKMuLtXhLeWhWGmrolr97CftFvnmOFEB4dcZbaPSGo0ecruCWVHJS7RYJiUGXh78QraJE0BDgli0EEEEoRq2qCENNbc4mDcjQOhlWZGASoyatuA1I6sk_WOuXrkwG0Tg3qIdpPe-lj4";

$jsonBody = file_get_contents('php://input');
switch ($jsonBody['request']['intent']['name']) {
  case 'AMAZON.CancelIntent':
  $textForAlexa = "thank you very much. I will wait for you soon in Processmaker";
  $response = '{
    "response": {
      "outputSpeech": {
        "type": "PlainText",
        "text": "' . $textForAlexa . '"
      },
      "shouldEndSession": true
    }
  }';
  echo $response;
  break;

  case 'AnswerAction':
  switch ($jsonBody['request']['intent']['slots']['Action']['value']) {
    case 'create user':
    $textForAlexa = 'Please, start with the phrase ADD USER then the first name and last name separated by the AND word';
    $response = '{
      "response": {
        "outputSpeech": {
          "type": "PlainText",
          "text": "' . $textForAlexa . '"
        },
        "shouldEndSession": false
      }
    }';
    echo $response;
    break;

    case 'list users':
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => $server . "/api/v1/users?page=1&perPage=1000",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "accept: application/json",
        $authorization,
        "cache-control: no-cache",
        "content-type: application/json",
        "postman-token: 8f5a758b-8b63-189e-3dbd-0ffb319dd398"
        ),
      ));

    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {
     $textForAlexa =  "cURL Error #:" . $err;
   } 
   else {
     $response = (json_decode($response,true));
     $textForAlexa = '';
     for ($i=0; $i < count($response['data']); $i++) {
       $textForAlexa .= "User: " . $response['data'][$i]['attributes']['username'];
       $textForAlexa .= ", First Name: " . $response['data'][$i]['attributes']['firstname'];
       $textForAlexa .= ", Last Name: " . $response['data'][$i]['attributes']['lastname'];
       $textForAlexa .= ", Status: " . $response['data'][$i]['attributes']['status'];
       $textForAlexa .= " . ";
     }
     $response = '{
      "response": {
        "outputSpeech": {
          "type": "PlainText",
          "text": "' . $textForAlexa . '"
        },
        "shouldEndSession": false
      }
    }';
    echo $response;
  }
  break;

  case 'list cases':
  $curl = curl_init();
  curl_setopt_array($curl, array(
    CURLOPT_URL => $server . "/api/v1/processes/" . $processId . "/instances",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
      $authorization,
      "cache-control: no-cache",
      "content-type: application/json",
      "postman-token: 348b274c-f7ff-4fc2-9f79-3d2b10cbd380"
      ),
    ));

  $response = curl_exec($curl);
  $err = curl_error($curl);

  curl_close($curl);

  if ($err) {
    $textForAlexa =  "cURL Error #:" . $err;
  } else {
          //echo $response;
    $response = (json_decode($response,true));
    $textForAlexa = '';
    $cont = 0;
    error_log(print_r($response['data'],true));
    for ($i=0; $i < count($response['data']); $i++) {
      $cont++;
      $textForAlexa .= "Process: Process Epic";
      $textForAlexa .= ", Status: " . $response['data'][$i]['attributes']['status'];
      $textForAlexa .= " . ";
    }

    $response = '{
      "response": {
        "outputSpeech": {
          "type": "PlainText",
          "text": "You Have ' . $cont . ' cases. ' . $textForAlexa . '"
        },
        "shouldEndSession": false
      }
    }';
    echo $response;
  }
  break;

  case 'create case':

  $curl = curl_init();

  curl_setopt_array($curl, array(
    CURLOPT_URL => $server . "/api/v1/processes/45d5ae54-f6d3-4c47-b1ef-ee15e8467d69/events/9915a488-24d1-4451-a7e9-80ca14f504ae/trigger",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 60,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_HTTPHEADER => array(
      $authorization,
      "content-type: application/json"
      ),
    ));

  $response = curl_exec($curl);
  $err = curl_error($curl);
  curl_close($curl);

  if ($err) {
    $textForAlexa = "cURL Error #:" . $err;
  } else {
    $textForAlexa = "The Case was created successfully.";
  }


  $response = '{
    "response": {
      "outputSpeech": {
        "type": "PlainText",
        "text": "' . $textForAlexa . '"
      },
      "shouldEndSession": false
    }
  }';
  echo $response;
  break;

  default:
  $textForAlexa = 'Please tell me some action.';
  $response = '{
    "response": {
      "outputSpeech": {
        "type": "PlainText",
        "text": "' . $textForAlexa . '"
      },
      "shouldEndSession": false
    }
  }';
  echo $response;
  break;
}

break;

case 'AnswerIntentCreateUser':    
$ArrayName = explode(' ', $jsonBody['request']['intent']['slots']['Answer']['value']);    
$firstName = $ArrayName[0];
$lastName = $ArrayName[1];
$curl = curl_init();
curl_setopt_array($curl, array(
  CURLOPT_URL => $server . "/api/v1/users",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => '{"data" : {  "type" : "user",  "attributes" : {    "email" : "' . $firstName . '.' . $lastName . '@processmaker.com",    "firstname" : "' . $firstName . '",    "lastname" : "' . $lastName . '",    "password" : "sample",    "username" : "' . $firstName . '.' . $lastName . '"  } }}',
  CURLOPT_HTTPHEADER => array(
    "accept: application/json",
    $authorization,
    "cache-control: no-cache",
    "content-type: application/json",
    "postman-token: 2a158cec-aacd-edfc-4507-7b76bf6b2d83"
    ),
  ));

$response = curl_exec($curl);

$err = curl_error($curl);

curl_close($curl);

if ($err) {  
  $textForAlexa = "cURL Error #:" . $err;
  
} else {
  $textForAlexa = "The user, " . $firstName . " " . $lastName . ", with email," . $firstName . '.' . $lastName . '@processmaker.com, was successfully created';
}
$response = '{
  "response": {
    "outputSpeech": {
      "type": "PlainText",
      "text": "' . $textForAlexa . '"
    },
    "shouldEndSession": false
  }
}';
echo $response;
break;  

default:
$textForAlexa = 'Hi hackaton Processmaker user, please tell me one of the following options starting with the key action. Create User. List User. Create Case. List Cases';
$response = '{
  "response": {
    "outputSpeech": {
      "type": "PlainText",
      "text": "' . $textForAlexa . '"
    },
    "shouldEndSession": false
  }
}';
echo $response;

break;
}
?>